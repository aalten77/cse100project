#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

void print(int * ary, int length){
	int i = 0;
	for(i = 0; i < length; i++){
		printf("%d\n", ary[i]);
	}
}

int parent(int i ){
	return (i-1)/2;
}

int left(int i){
	return (2*i)+1;
}

int right(int i){
	return (2*i)+2;
}

void maxHeapify(int * ary, int i, int length){
	int l = left(i);
	int r = right(i);
	int temp = 0;
	int largest;

	if(l < length && ary[l] > ary[i]){
		largest = l;
	}
	else
		largest = i;
	
	if(r < length && ary[r] > ary[largest])
		largest = r;
	if(largest != i){
		temp = ary[i];
		ary[i] = ary[largest];
		ary[largest] = temp;
		maxHeapify(ary, largest, length);
	}
}

void buildMaxHeap(int * ary, int length){
	int i = 0;
	for(i = (length-1)/2; i >= 0; i--){
		maxHeapify(ary, i, length);
	}
}

void heapsort(int * ary, int length){
	buildMaxHeap(ary, length);
	int heapSize = length;
	int i = 0;
	for(i = length-1; i>= 1; i--){
		int temp = ary[0];
		ary[0] = ary[i];
		ary[i] = temp;
		heapSize -= 1;
		maxHeapify(ary, 0, heapSize);
	}
}


void mysort(int* ary, int length, int k) {
	heapsort(ary, length);
}


/*int main(){
	int length = 0;
	int * numbers;
	int i = 0;
	scanf("%d", &length);
	while(length != 0){
		i = 0;
		numbers = (int*) malloc(length*sizeof(int)); 
		while(i < length){
			scanf("%d", &numbers[i]);
			i++;
		}
		heapsort(numbers, length);			
		print(numbers, length);
		
		scanf("%d", &length);
	}
}
*/
