#!/bin/sh

sh ./CountingSort/sortscript.sh CountingSort/SortTime 100128538 > outputCounting.txt
sh ./RadixSort/sortscript.sh RadixSort/SortTime 100128538 > outputRadix.txt
sh ./BubbleSort/sortscript.sh BubbleSort/SortTime 100128538 > outputBubble.txt
sh ./HeapSort/sortscript.sh HeapSort/SortTime 100128538 > outputHeap.txt
sh ./InsertionSort/sortscript.sh InsertionSort/SortTime 100128538 > outputInsertion.txt
sh ./MergeSort/sortscript.sh MergeSort/SortTime 100128538 > outputMerge.txt
sh ./QuickSort/sortscript.sh QuickSort/SortTime 100128538 > outputQuick.txt
sh ./SelectionSort/sortscript.sh SelectionSort/SortTime 100128538 > outputSelection.txt
