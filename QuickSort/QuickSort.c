// Michael McBride
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

void QuickSort(int *A, int l, int h) {
    // subdivide problem while input is larget than 1
    if (l < h) {
        int pivotPos = Partition(A, l, h);
        QuickSort(A, l, pivotPos - 1);
        QuickSort(A, pivotPos + 1, h);
    }
}

int Partition(int *A, int l, int h) {
    int i, temp;
    int randompiv = (rand() % (h-l+1)) + l;
    temp = A[randompiv];
    A[randompiv] = A[h];
    A[h] = temp;

    int pivot = A[h];
    int pivotPos = l-1;

    // iterate through array subsection and separate around pivot
    for (i = l; i <= h-1; i++) {
        if (A[i] < pivot) {
            pivotPos++;
            temp = A[i];
            A[i] = A[pivotPos];
            A[pivotPos] = temp;
        }
    }
    temp = A[h];
    A[h] = A[pivotPos+1];
    A[pivotPos+1] = temp;

    return pivotPos+1;
}

void mysort(int* ary, int length, int k) {
	QuickSort(ary, 0, length-1);
}
