#include <limits.h>
#include <stdio.h>
#include <stdlib.h>


void print(int * ary, int length){
	int i = 0;
	for(i = 0; i < length; i++){
		printf("%d ", ary[i]);
		//cout << ary[i] << endl;
	}
}


void merge(int * ary, int l, int mid, int r){
	int * left;
	int * right;
	int lengthL = mid - l + 1;
	int lengthR = r - mid;

	left = (int*)malloc((lengthL+1)*sizeof(int));//new int[lengthL+1];
	right = (int*)malloc((lengthR+1)*sizeof(int));//new int[lengthL+1];
	//right = new int[lengthR+1];

	int i, j, k;

	for(i = 0; i < lengthL; i++){
		left[i] = ary[l+i];		
	}

	for(j = 0; j < lengthR; j++){
		right[j] = ary[mid+j+1];
	}
 	
	left[i] = INT_MAX;
	right[j] = INT_MAX;
	i = 0;
	j = 0;

	for(k = l; k <= r; k++){
		if(left[i] <= right[j]){
			ary[k] = left[i];
			i++;
		}
		else{
			ary[k] = right[j];
			j++;
		}
	}

	free(left);
	free(right);
}

void mergeSort(int * ary, int l, int r){
	if(l < r){
		int mid = (l+r)/2;
		mergeSort(ary, l, mid);
		mergeSort(ary, mid+1, r);
		merge(ary, l, mid, r);	
	}
	return;
}

void mysort(int* ary, int n, int k) {
	mergeSort(ary, 0, n-1);
}

/*int main(){
	int length = 0;
	int * numbers;
	int i = 0;
	scanf("%d", &length);
	while(length != 0){
		i = 0;
		numbers = (int*) malloc(length*sizeof(int));//new int[length];
		while(i < length){
			scanf("%d", &numbers[i]);
			i++;
		}
		mergeSort(numbers, 0, length-1);
		print(numbers, length);

		scanf("%d", &length);
	}
}*/
