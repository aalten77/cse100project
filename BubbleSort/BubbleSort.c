// Michael McBride
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

void BubbleSort(int *A, int n) {
    int i, j;

    // iterate through array A swapping locations with their successor when they are larger
    for (i = 0; i < n - 1; i++) {
        for (j = 0; j < n - 1; j++) {
            if (A[j] > A[j + 1]) {
                int temp = A[j];
                A[j] = A[j + 1];
                A[j + 1] = temp;
            }
        }
    }
}

void mysort(int* ary, int n, int k) {
	BubbleSort(ary, n);
}
