// Michael McBride
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <string.h>

void CountingSort(int *A, int *B, int n, int k) {
    int *C;
	int i = 0;
    C = (int *)malloc(sizeof(int) * k);

    // fill array C with 0
    for (i = 0; i < k; i++) {
        C[i] = 0;
    }
    // add each occurrence of A[i] into a C slot
    for (i = 0; i < n; i++) {
		C[A[i]] = C[A[i]] + 1;
	}
    // sum each C[i] with C[i - 1]
	for (i = 1; i < k; i++) {
		C[i] = C[i] + C[i - 1];
	}
    // pull entries from array A in order into array B with C as the ordermaker
    for (i = n - 1; i >= 0; i--) {
		B[C[A[i]] - 1] = A[i];
		C[A[i]] = C[A[i]] - 1;
	}
    free(C);
}

mysort(int* ary, int length, int k) {
	int* ary2 = (int*)malloc(sizeof(int)*length);
	CountingSort(ary, ary2, length, k+1);
	memcpy(ary, ary2, sizeof(int) * length);
	free(ary2);
}
