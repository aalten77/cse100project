#include <stdlib.h>
#include <stdio.h>

#define RADIXSIZE 10

void radixSort(int * aryA, int size, int place){
	int rem = RADIXSIZE;
	int divisor = place;
	int i = 0;
	int j = 0;
	if(divisor == 0) {
		divisor = 1;
	}
	
	int * aryC = (int*)malloc(size*sizeof(int)); // new array with length of largest element in A + 1
	int * aryB = (int*)malloc(RADIXSIZE*sizeof(int));
	
	for(i = 0; i < RADIXSIZE; i++)
		aryB[i] = 0;	
	for(j = 0; j < size; j++){
		aryB[(int)(aryA[j]/divisor)%rem]++;	
	}
	
	for(i = 1; i < RADIXSIZE; i++){
		aryB[i] += aryB[i-1];	
	}

	for(j = size-1; j >= 0; j--){
		aryC[aryB[(int)(aryA[j]/divisor) % rem] - 1] = aryA[j];
		aryB[(int)(aryA[j]/divisor) % rem]--;
	}
	for(j = 0; j < size; j++) //copy aryC to aryA
		aryA[j] = aryC[j];
}

void mysort(int* ary, int length, int k) {
	int i = 0;
	int exp = 0;
	int big = ary[0];
                for(i = 1; i < length; i++){ // find biggest element
                        if(ary[i] > big)
                                big = ary[i];
                }
                for(exp = 1; (int)(big/exp) > 0; exp*=10){
                        radixSort(ary, length, exp);
                }
}

/*int main(){
	int size;
	scanf("%d", &size);
	int i = 0;
	int exp = 0;
	while(size != 0){
		int * aryA = (int*)malloc(size*sizeof(int));
		int k; // largest element
		for(i = 0; i < size; i++){
			scanf("%d", &aryA[i]);
		}	
		k = aryA[0];
		for(i = 1; i < size; i++){ // find biggest element
			if(aryA[i] > k)
				k = aryA[i];
		}
		for(exp = 1; (int)(k/exp) > 0; exp*=10){
			radixSort(aryA, size, exp);
		}
		
		for(i = 0; i < size; i++)
			printf("%d ", aryA[i]);
		scanf("%d", &size);
	}
}*/
