// Michael McBride
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

void SelectionSort(int *A, int n) {
    int i, j, key, keyPos;
    // select lowest remaining value and place in current offset
    for (i = 0; i < n - 1; i++) {
        key = A[i];
        keyPos = i;
        for (j = i + 1; j < n - 1; j++) {
            if (A[j] < key) {
                key = A[j];
                keyPos = j;
            }
        }
        A[keyPos] = A[i];
        A[i] = key;
    }
    //return A;
}

void mysort(int* ary, int length, int k) {
	SelectionSort(ary, length);
}
