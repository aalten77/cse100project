#include <stdio.h>
#include <stdlib.h>

void print(int * ary, int length){
	int i = 0;
	for(i = 0; i < length; i++){
		printf("%d ", ary[i]);
	}
	printf("\n");
}

void insertionSort(int * ary, int length){
	int key = 0;
	int i = 0, j = 0;
	for(j = 1; j < length; j++){
		key = ary[j];
		i = j-1;
		while(i >= 0 && ary[i] > key){
			ary[i+1] = ary[i];
			i--;
		}
		ary[i+1] = key;
	}
}

mysort(int* ary, int length, int k) {
	insertionSort(ary, length);
}

/*int main(){
	int * numbers;
	int length = 0;	
	scanf("%d", &length);
	while(length != 0){
		numbers = (int *) malloc(length*sizeof(int));
		int i = 0;
		for(i = 0; i < length; i++){
			scanf("%d", &numbers[i]);
		}
		insertionSort(numbers, length);
		print(numbers, length);
		scanf("%d", &length);
	}
	return 0;
}*/
